/* 
 * File:   Dataset.h
 * Author: lechszym
 *
 * Created on 26 April 2015, 10:07 AM
 */

#ifndef DATASET_H
#define	DATASET_H

#include "Matrix.h"

namespace dlearn {

class Dataset {
public:
   Dataset();
   Dataset(const Dataset& orig);
   virtual ~Dataset();

   size_t nInputs() const;
   size_t nParams() const;
   
   dmatrix::Matrix             x();
   dmatrix::Matrix             t();
   
protected:   
   Dataset(size_t xDim, size_t tDim, size_t N);
   
   dmatrix::Matrix             X;
   dmatrix::Matrix             T;


   
};
}


#endif	/* DATASET_H */

