/* 
 * File:   Block.h
 * Author: lechszym
 *
 * Created on 26 April 2015, 10:08 AM
 */

#ifndef FBLOCK_H
#define	FBLOCK_H

#include "Dataset.h"
#include <vector>

namespace dlearn {

class finit;
class fblock_w;
class fblock;   
class fblock_array;   




#ifdef BOOST_ENABLED
typedef boost::shared_ptr<fblock_w> fblock_w_ptr;    
#else
typedef std::shared_ptr<fblock_w> fblock_w_ptr;
#endif   

class finit {
   
public:
   finit();
   virtual ~finit();

   void _init_(fblock& f);
   void _init_(fblock& f, const dmatrix::Matrix& X);
   void _init_(fblock& f, const dmatrix::Matrix& X, const dmatrix::Matrix& T);

protected:   
   virtual void init(fblock& f);
   virtual void init(fblock& f, const dmatrix::Matrix& X);
   virtual void init(fblock& f, const dmatrix::Matrix& X, const dmatrix::Matrix& T);
   
   finit operator>>(finit& f) const;
   
   finit*                  _fin=NULL;
};

class fblock_w {

public: 
   fblock_w();
   virtual ~fblock_w();

   dmatrix::Vector   _w;
};


class fblock {
  
public: 
   fblock();
   fblock(const fblock& orig);
   virtual ~fblock();

   size_t nInputs() const;
   size_t nOutputs() const;
   size_t nParams() const;
   
   dmatrix::Vector w();

   void init(const dmatrix::Matrix& X, const dmatrix::Matrix& T);
   
   dmatrix::Matrix output(const dmatrix::Matrix& X);
   void            output(const dmatrix::Matrix& X, dmatrix::Matrix& Y);
   dmatrix::Matrix output(const dmatrix::Matrix& X, const dmatrix::Vector& w);
   void            output(const dmatrix::Matrix& X, const dmatrix::Vector& w, dmatrix::Matrix& Y);
   
   void f(const dmatrix::Matrix& X, dmatrix::Matrix& Y);
   void dfdx(const dmatrix::Matrix& X, const dmatrix::Matrix &Y, dmatrix::Matrix& dYdX);    


   virtual void f(const dmatrix::Vector& w, const dmatrix::Matrix& X, dmatrix::Matrix& Y);
   virtual void dfdx(const dmatrix::Vector& w, const dmatrix::Matrix& X, const dmatrix::Matrix &Y, dmatrix::Matrix& dYdX);

   fblock_array operator+(const fblock& f) const;
   fblock operator>>(fblock& f) const;

   void _init_phase1(const dmatrix::Matrix& X);
   void _init_phase2(dmatrix::Vector& w, const dmatrix::Matrix& X, const dmatrix::Matrix& T);   
   
protected:
   size_t                   _outputs = 0;
   size_t                   _inputs = 0;
   size_t                   _params = 0;
   fblock_w_ptr             _w;
   bool                     _initialised=false;
   fblock*                  _fin=NULL;
   
   virtual size_t _nParams() const;
   
   void set_w(const dmatrix::Vector &w);
   
   finit*                   _finit=NULL;
};


class fblock_array: public fblock {

public:
   fblock_array(const fblock& f1, const fblock& f2);
   fblock_array(const fblock_array& orig);
   
   fblock_array operator+(const fblock& f) const;   
   fblock_array operator+(const fblock_array& f) const;
   
private:
   std::vector<fblock>      _farray;
   size_t _nParams() const;   
   
};


class fblock_pass: public fblock {
   
public:
   fblock_pass();
   fblock_pass(const fblock_pass& orig);
   virtual ~fblock_pass();

   dmatrix::Matrix output(const dmatrix::Matrix& X);
   void            output(const dmatrix::Matrix& X, dmatrix::Matrix& Y);
   dmatrix::Matrix output(const dmatrix::Matrix& X, const dmatrix::Vector& w);
   void            output(const dmatrix::Matrix& X, const dmatrix::Vector& w, dmatrix::Matrix& Y);

   size_t nInputs() const;
   size_t nOutputs() const;
   size_t nParams() const;

protected:   
   void _init_phase1(const dmatrix::Matrix& X);
   void _init_phase2(dmatrix::Vector& w, const dmatrix::Matrix& X, const dmatrix::Matrix& T);   
};

}

#endif	/* FBLOCK_H */

