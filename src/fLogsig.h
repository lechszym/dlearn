/* 
 * File:   fLogsig.h
 * Author: lechszym
 *
 * Created on 10 May 2015, 9:36 PM
 */

#ifndef FLOGSIG_H
#define	FLOGSIG_H

#include "fblock.h"

namespace dlearn {

class fLogsig : public fblock {
public:
   
   fLogsig(double L=1.0, double k=1.0, double x0=0.0); 
   fLogsig(const fLogsig& orig);
   
   void f(const dmatrix::Vector& w, const dmatrix::Matrix& X, dmatrix::Matrix& Y);
   void dfdx(const dmatrix::Vector& w, const dmatrix::Matrix& X, const dmatrix::Matrix &Y, dmatrix::Matrix& dY);
   
private:
   double _L;
   double _k;
   double _x0;
};

}

#endif	/* FLOGSIG_H */

