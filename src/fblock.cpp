/* 
 * File:   Block.cpp
 * Author: lechszym
 * 
 * Created on 26 April 2015, 10:08 AM
 */

#include "fblock.h"

using namespace dmatrix;

namespace dlearn {

finit::finit() {
}
   
finit::~finit() {
   if(_fin) {
      delete(_fin);
   }
}

void finit::init(fblock& f) {

}

void finit::init(fblock& f, const dmatrix::Matrix& X) {
   this->init(f);   
}

void finit::init(fblock& f, const dmatrix::Matrix& X, const dmatrix::Matrix& T) {
   this->init(f);
}
   
void finit::_init_(fblock& f) {
   if (f.nParams() > 0) {
      if(_fin) {
         _fin->_init_(f);
      }
      init(f);
   }
}

void finit::_init_(fblock& f, const dmatrix::Matrix& X) {
   if (f.nParams() > 0) {
      if(_fin) {
         _fin->_init_(f, X);
      }
      init(f, X);
   }  
}

void finit::_init_(fblock& f, const dmatrix::Matrix& X, const dmatrix::Matrix& T) {
   if (f.nParams() > 0) {
      if(_fin) {
         _fin->_init_(f, X, T);
      }
      init(f, X, T);
   }  
}
   
finit finit::operator>>(finit& f) const {

   finit result = f;
   finit* fin = new finit(*this);
   
   if(!fin) {
      //ERROR
   }

   
   if(result._fin) {
      // if f already has input, this becomes input of f._fin
      result._fin = new finit(*fin >> *result._fin);
      if(!result._fin) {
         //ERROR
      }
      
   } else {
      result._fin = fin;
   }
   
   return result;
}   



   
fblock::fblock() {

}
   
fblock::fblock(const fblock& orig) {
   _outputs = orig._outputs;
   _inputs = orig._inputs;
   _params = orig._params;
   _w = orig._w;
   _initialised=orig._initialised;
   if(orig._fin) {
      _fin = new fblock(*orig._fin);
   }
   if(orig._finit) {
      _finit = new finit(*orig._finit);
   }
}

fblock::~fblock() {
   if(_fin) {
      delete(_fin);
   }   
   if(_finit) {
      delete(_finit);
   }
}

size_t fblock::_nParams() const {
   return _params;
}


size_t fblock::nInputs() const {
   if(_fin) {
      return _fin->nOutputs();
   } else {
      return _inputs;
   }
}

size_t fblock::nOutputs() const {
    return _outputs;
}

size_t fblock::nParams() const {
    size_t params = this->_nParams();
    if(_fin) {
        params += _fin->nParams();
    }
    return params;
}

Vector fblock::w() {
   return _w->_w;
}

 void fblock::set_w(const Vector &w) {
    _w->_w = w;
 }

 
 
Matrix fblock::output(const Matrix& X) {
   return this->output(X,this->w());   
}

void fblock::output(const Matrix& X, Matrix& Y) {
   this->output(X,this->w(),Y);     
}
   
Matrix fblock::output(const Matrix& X, const Vector& w) {
   Matrix Y = Matrix(this->nInputs(),this->nOutputs());
   this->output(X,w,Y);
   return Y;
}

void fblock::output(const Matrix& X, const Vector& w, Matrix& Y) {
   Matrix _X;

   if(_fin) {
      size_t params = this->_nParams();
      _X = _fin->output(X, Vector(w,w.size-params,params));
   } else {
      _X = X;
   }
    
   //if(_farray.empty()) {
   this->f(w,_X,Y);
   /*} else {
      size_t i1 = 0;
      size_t o1 = 0;
      for(size_t i=0;i<_farray.size();i++) {
         size_t i2 = i1+_farray[i].nInputs();
         size_t o2 = o1+_farray[i].nOutputs();
         
         Matrix Xsub = Matrix(_X,i1,i2,0,_X.cols);
         Matrix Ysub = Matrix(Y,o1,o2,0,Y.cols);
         size_t params = this->_nParams();
         Vector wsub = Vector(w,w.size-params,params);
                 
         _farray[i].output(Xsub,wsub,Ysub);
         i1 = i2;
         o1 = o2;
      }
   }*/
}


void fblock::f(const Matrix& X, Matrix& Y) {
   this->f(this->w(),X, Y);
}

void fblock::f(const Vector& w, const Matrix& X, Matrix& Y) {
   Y.set_zero();
}

void fblock::dfdx(const Matrix& X, const Matrix &Y, Matrix& dYdX) {
   this->dfdx(this->w(), X, Y, dYdX);
}


void fblock::dfdx(const Vector& w, const Matrix& X, const Matrix &Y, Matrix& dYdX) {
   dYdX.set_zero();
}

void fblock::init(const Matrix& X, const Matrix& T) {
   _outputs = T.rows;
   this->_init_phase1(X);
   Vector w = Vector(this->nParams());
   this->_init_phase2(w, X, T);   
}

void fblock::_init_phase1(const Matrix& X) {
   if(_fin) {
      _fin->_init_phase1(X);
      _inputs = _fin->_outputs;
   } else {
      _inputs = X.rows;
   }

   if(_outputs == 0) {
      _outputs = _inputs;
   }
}


void fblock::_init_phase2(Vector& w, const Matrix& X, const Matrix& T) {
   size_t params = this->nParams();
   Matrix _X;

   if(params > 0) {
      Vector wparams = Vector(w, w.size - params, params);
   
      if(_initialised) {
         wparams.copy(this->w());
      }
      this->set_w(wparams);
   }
      
   if(_fin) {
      Vector win = Vector(w, 0, w.size-params);
      _fin->_init_phase2(win, X, T);
      _X = Matrix(_fin->_outputs, X.cols);
      _fin->output(X, _X);
   } else {
      _X = X;
   }
   
   if(params > 0 && _finit) {
      _finit->_init_(*this, _X, T);
   }
   _initialised = true;
}


   
   /*Vector w;
   if(_farray.empty()) {
      size_t paramsloc = this->_nParams();
      w = Vector(win.size+paramsloc);
      Vector wloc = Vector(w,win.size,w.size-paramsloc);
      this->_initialise(wloc, _X, T);
      Vector(w,0,win.size).copy(win);
   } else {
      
   }*/
   
   //return w;


fblock_array fblock::operator+(const fblock& f) const {
   return fblock_array(*this, f);
}

fblock fblock::operator>>(fblock& f) const {

   //this becomes input of f
   
   fblock result = f;
   fblock* fin = new fblock(*this);
   
   if(!fin) {
      //ERROR
   }

   
   if(result._fin) {
      // if f already has input, this becomes input of f._fin
      result._fin = new fblock(*fin >> *result._fin);
      if(!result._fin) {
         //ERROR
      }
      
   } else {
      result._fin = fin;
   }
   
   return result;
}








 fblock_array::fblock_array(const fblock& f1, const fblock& f2) {
   _farray.push_back(f1);
   _farray.push_back(f2);
 }

   
fblock_array::fblock_array(const fblock_array& orig) : fblock(orig) {
   for(size_t i=0;i<orig._farray.size();i++) {
      _farray.push_back(orig._farray[i]);
   }
   
}

size_t fblock_array::_nParams() const {
   size_t params = 0;
   for(size_t i=0;i<_farray.size();i++) {
      params += _farray[i].nParams();
   }
   return params;
}

fblock_array fblock_array::operator+(const fblock& f) const {
   fblock_array result = fblock_array(*this);
   result._farray.push_back(f);
   return result;
}

fblock_array fblock_array::operator+(const fblock_array& f) const {
   fblock_array result = fblock_array(*this);
   for(size_t i=0;i<f._farray.size();i++) {
      result._farray.push_back(f._farray[i]);
   }
   return result;
}



fblock_pass::fblock_pass() : fblock() {
   _initialised = true;
}
   
fblock_pass::fblock_pass(const fblock_pass& orig) : fblock(orig) {

}

fblock_pass::~fblock_pass() {
   
}

 
Matrix fblock_pass::output(const Matrix& X) {
   if(!_fin) {
      //Error
   }
   return _fin->output(X);   
}

void fblock_pass::output(const Matrix& X, Matrix& Y) {
   if(!_fin) {
      //Error
   }
   _fin->output(X,Y);   
}
   
Matrix fblock_pass::output(const Matrix& X, const Vector& w) {
   if(!_fin) {
      //Error
   }
   return _fin->output(X,w);
}

void fblock_pass::output(const Matrix& X, const Vector& w, Matrix& Y) {
   if(!_fin) {
      //Error
   }
   _fin->output(X,w,Y);
}

size_t fblock_pass::nInputs() const {
   if(!_fin) {
      //Error
   }
   return _fin->nInputs();
}

size_t fblock_pass::nOutputs() const {
   if(!_fin) {
      //Error
   }
   return _fin->nOutputs();   
}

size_t fblock_pass::nParams() const {
   if(!_fin) {
      //Error
   }
   return _fin->nParams();      
}

void fblock_pass::_init_phase1(const Matrix& X) {
   if(!_fin) {
      //Error
   }
   _fin->_init_phase1(X);   
}


void fblock_pass::_init_phase2(Vector& w, const Matrix& X, const Matrix& T) {
   if(!_fin) {
      //Error
   }
   _fin->_init_phase2(w, X, T);
   _initialised = true;
}


/*
void fblock_array::f(const Matrix& X, const Vector& w, Matrix& Y) {
      size_t i1 = 0;
      size_t o1 = 0;
      for(size_t i=0;i<_farray.size();i++) {
         size_t i2 = i1+_farray[i].nInputs();
         size_t o2 = o1+_farray[i].nOutputs();
         
         Matrix Xsub = Matrix(_X,i1,i2,0,_X.cols);
         Matrix Ysub = Matrix(Y,o1,o2,0,Y.cols);
         size_t params = this->_nParams();
         Vector wsub = Vector(w,w.size-params,params);
                 
         _farray[i].output(Xsub,wsub,Ysub);
         i1 = i2;
         o1 = o2;
      }
   }
}*/

/*
//if (dynamic_cast<D2*>(x) == NULL)

   
Block::Block() : Block(0,0,0) {

}

Block::Block(size_t nInputs, size_t nOutputs, size_t nParams) {
   _params = nParams;
   _inputs = nInputs;
   _outputs = nOutputs;
}


Block::Block(const Block& orig) : Block(orig._inputs,orig._outputs,orig._params) {
   for(size_t i=0;i<orig._blocks.size();i++) {
      _blocks.push_back(orig._blocks[i]);      
   }
   _shell = orig._shell;
}

Block::~Block() {
}


void Block::setInputs(size_t inputs) {
   if(_inputs != 0 && _inputs != inputs) {
      std::wcout << "Error! Can't reconfigure " << _inputs << "-input" << 
                 " block to " << inputs << "-input block!!!\n";
         exit(-1);
   }
   _inputs = inputs;
}


size_t Block::nInputs() const {
   if(_blocks.empty()) {
      return _inputs;
   } else {
      size_t inputs = 0;
      
      for(size_t i=0;i<_blocks.size();i++) {
         inputs += _blocks[i]._outputs;
      }
      return inputs;
   }
}

size_t Block::nOutputs() const {
   if(_outputs == 0) {
      size_t outputs = 0;
      for(size_t i=0;i<_blocks.size();i++) {
         outputs += _blocks[i].nOutputs();
      }      
      return outputs;
   } else {
      return _outputs;
   }
}

size_t Block::nParams() const {
   size_t params = _params;
   for(size_t i=0;i<_blocks.size();i++) {
      params += _blocks[i].nParams();
   }      
   return params;
}

Matrix Block::out(const Vector& w, const Matrix& X) {
   Matrix Y = Matrix(_outputs,X.cols);
   this->_out(w,X,Y);
   return Y;
}


void Block::_out(const Vector &w, const Matrix& X, Matrix& Y) {
   size_t nOutputs = _outputs;
   size_t nPoints = X.cols;
   
   
   if(Y.rows != nOutputs) {
     std::wcout << "Error! Can't write data of " << nOutputs << 
             "-output model to a " << Y.rows << "-row output matrix!!!\n";
         exit(-1);
   }

   if(Y.cols != nPoints) {
     std::wcout << "Error! Can't process a " << nPoints << 
             "-point data input with a " << Y.cols <<
             "-column output matrix!!!\n";
         exit(-1);
   }

   if(_blocks.empty()) {
      if(X.rows != _inputs) {
        std::wcout << "Error! Can't process a " << X.rows << 
             "-dim data input with a " << _inputs <<
             "-dim input block!!!\n";
         exit(-1);         
      }
      this->f(w,X,Y);
   } else {
      size_t nInputs = this->nInputs();

      if(_Xcache.rows < nInputs || _Xcache.cols < nPoints) {
         _Xcache = Matrix(nInputs,nPoints);
      }

      size_t offset = 0;
      size_t n = w.size-_params;
      for(size_t i=0;i<_blocks.size();i++) {
         Matrix blkY = Matrix(_Xcache,offset,_blocks[i]._outputs);
         Block *blk = &_blocks[i];
         blk->_out(Vector(w,0,n),X,blkY);
         offset += blk->_outputs;
         n += blk->nParams();
      }

      this->f(Vector(w,w.size-_params,_params),_Xcache,Y);
   } 
}

void Block::f(const Matrix& X, Matrix& Y) {
   Y.set_zero();
}

void Block::f(const Vector& w, const Matrix& X, Matrix& Y) {
   this->f(X,Y);
}

void Block::dfdx(const Matrix& X, const Matrix& Y, Matrix& dYdX) {
   dYdX.set_zero();
}

void Block::dfdx(const Vector& w, const Matrix& X, const Matrix& Y, Matrix &dYdX) {
   this->dfdx(X,Y,dYdX);
}

void Block::state(const Matrix& Y, Matrix &S) {
   
}

void Block::update(const Vector& dw, Vector &w) {
   w += dw;
}

Block Block::operator+(const Block& blk) const {
   Block shell;
   
   if(_shell) {
      for(size_t i=0;i<_blocks.size();i++) {
          shell._blocks.push_back(_blocks[i]);               
      }     
   } else {
      shell._blocks.push_back(*this);
   }

   if(blk._shell == 0) {
      for(size_t i=0;i<blk._blocks.size();i++) {
          shell._blocks.push_back(blk._blocks[i]);               
      }           
   } else {
      shell._blocks.push_back(blk);
   }
   shell._shell = true;
   
   return shell;
}

Block Block::operator>>(const Block& blk) const {
   Block result = blk;
   size_t outputs = nOutputs();
   size_t inputs = result.nInputs();
   
   if(result._blocks.empty()) {
      if(inputs==0) {
         //Input of the block will be dynamically adjusted
         result.setInputs(outputs);
         inputs = result.nInputs();
      } else if(outputs != inputs) {
         std::wcout << "Error! Can't connect " << outputs << "-output" << 
                 " block to " << inputs << "-input block!!!\n";
         exit(-1);         
      }
      if(_outputs == 0) {
         for(size_t i=0;i<_blocks.size();i++) {
            result._blocks.push_back(_blocks[i]);
         }
      } else {
         result._blocks.push_back(*this);
      }
   } else if(outputs == inputs) {
        std::wcout << "Error! Output to multiple inputs not supported at this time!!!\n";
        exit(-1);    
   } else {
        std::wcout << "Error! Can't connect " << outputs << "-output" << 
                      " block to " << inputs << "-input block!!!\n";
        exit(-1);    
   }
   
   
   
   
   return result;
}
*/
}