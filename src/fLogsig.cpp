/* 
 * File:   fLogsig.cpp
 * Author: lechszym
 * 
 * Created on 10 May 2015, 9:36 PM
 */

#include "fLogsig.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_exp.h"

using namespace dmatrix;

namespace dlearn {

fLogsig::fLogsig(double L, double k, double x0) {
   _L = L;
   _k = k;
   _x0 = 0;
}

fLogsig::fLogsig(const fLogsig& orig) : fblock(orig) {
   _L = orig._L;
   _k = orig._k;
   _x0 = orig._x0;
 }
/* 
 
 * f(x) = L/(1-k*exp(-x-x0))
 
 */

void fLogsig::f(const dmatrix::Vector& w, const Matrix& X, Matrix &Y) {

   Matrix::elementwise(Y,X,_L,[=](double y, double x, double L) -> double {
      x = -_k*(x-_x0);
      if (x > GSL_LOG_DBL_MAX) {
         return L;
      } else if (x < GSL_LOG_DBL_MIN) {
         return 0.0;
      } else {
         double b = gsl_sf_exp(x);
         return L / (1 + b);
      }
   });
}

/* 
 
 * dfdx(x,y) = k*y*(1-y/L)
 
 */

void fLogsig::dfdx(const dmatrix::Vector& w, const Matrix& X, const Matrix &Y, Matrix& dYdX) {
   
   if(_L == 1.0 && _k == 1.0) {
      Matrix::elementwise(dYdX,Y,0.0,[](double dydx, double y, double L) -> double {
         return y * (1 - y);
      });
   } else {
      Matrix::elementwise(dYdX,X,_L,[=](double dydx, double x, double L) -> double {
           x = -_k*(x-_x0);
           double y;
           if (x > GSL_LOG_DBL_MAX) {
               return 0.0;
           } else if (x < GSL_LOG_DBL_MIN) {
               return 0.0;
           } else {
               y = gsl_sf_exp(x);
               y = 1 / (1 + y);
               return L*_k*(y-y*y);
           }       
      });
   }
}


}