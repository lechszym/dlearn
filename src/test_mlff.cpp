/* 
 * File:   test_mlff.cpp
 * Author: lechszym
 *
 * Created on 9 May 2015, 11:12 AM
 */

#include <cstdlib>

#include "fblock.h"
#include <iostream>
#include <ctime>

using namespace dlearn;
using namespace std;

#include "dIris.h"
#include "fFF.h"

/*
 * 
 */
int main(int argc, char** argv) {

   dIris data;
   
   //cout << data.x() << endl;
   //cout << data.t() << endl;
   
   fFF<fLogsig> net(7);
   //fMLFF net(vector<size_t>{8,4},fLogsig());
   
   
   //cout << "Output:" << net.out(data.X) << endl;
   
   
   return 0;
}

