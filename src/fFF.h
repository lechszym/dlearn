/* 
 * File:   fFF.h
 * Author: lechadmin
 *
 * Created on November 24, 2015, 3:59 PM
 */

#ifndef FFF_H
#define	FFF_H

#include "fLin.h"
#include "fLogsig.h"

template<class T>
class fFF: public fblock_pass {
public:
   fFF(size_t nOutputs) {
      fLin f1(nOutputs);
      T f2;

      _fin = new fblock(f1 >> f2);      
   }

   private:

};

#endif	/* FFF_H */

