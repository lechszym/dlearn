/* 
 * File:   dIris.cpp
 * Author: lechszym
 * 
 * Created on 12 May 2015, 8:53 PM
 */

#include "dIris.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace dmatrix;

namespace dlearn {

   dIris::dIris() : Dataset(4, 3, 150) {

      //size_t _outputs = 4;
      
      const string firisName = "../data/iris/iris.data";
      ifstream firis(firisName.c_str());
      if (!firis.is_open()) {
         cout << "Error! Failed to open " << firisName << " for read!!!\n";
         exit(-1);
      }

      const int dataBufSize = 256;
      char dataBuf[dataBufSize];
      char *token;
      T.set_zero();
      for (int i = 0; i < 150; i++) {
         double xData;

         if (!firis.getline(dataBuf, dataBufSize)) {
            cout << "Error! Failed to read data from " << firisName << "!!!\n";
            exit(-1);
         }
         token = strtok(dataBuf, ",");
         for (unsigned int j = 0; j < 4; j++) {
            if (token == NULL) {
               cout << "Error! Uncrecognized data format in " << firisName << "!!!\n";
               exit(-1);
            }
            xData = strtod(token, NULL);
            X(j, i) = xData;
            token = strtok(NULL, ",");
         }
         if (strcmp(token, "Iris-setosa") == 0) {
            T(0, i) = 1.0;
         } else if (strcmp(token, "Iris-virginica") == 0) {
            T(1, i) = 1.0;
         } else if (strcmp(token, "Iris-versicolor") == 0) {
            T(2, i) = 1.0;
         } else {
            cout << "Error! Uncrecognized label format in " << firisName << "!!!\n";
            exit(-1);
         }
      }
   }

dIris::dIris(const dIris& orig) {
}

dIris::~dIris() {
}

}