#include "Matrix.h"
#include <iostream>
#include <ctime>

using namespace dlearn;
using namespace std;

int main(void) {
   
   time_t start, end, elapsed;
   
   Matrix A(4000,4000);
   Matrix B(4000,4000);
   Matrix C(4000,4000);
   
   A.set_all(1.0);
   B.set_all(2.0);
   
   start = time(NULL);

   Matrix::dgemm(1.0,A,B,0.0,C);
 
   end = time(NULL);

   elapsed = end - start;

   cout << "C:" << Matrix(C,0,0,10,10) << "\n";
   
   printf("Elapsed time is %02d:%02d:%02d (hh:mm:ss)\n", 
           (int) elapsed / 3600, 
           (int) (elapsed % 3600) / 60, 
           (int) (elapsed % 3600) % 60);

   return 0;
}