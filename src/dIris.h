/* 
 * File:   dIris.h
 * Author: lechszym
 *
 * Created on 12 May 2015, 8:53 PM
 */

#ifndef DIRIS_H
#define	DIRIS_H

#include "Dataset.h"

namespace dlearn {
class dIris : public Dataset {
public:
   dIris();
   dIris(const dIris& orig);
   virtual ~dIris();
private:

};
}

#endif	/* DIRIS_H */

