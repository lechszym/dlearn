#include "Matrix.h"
#include <iostream>

using namespace dmatrix;
using namespace std;

int main(void) {
   
   Matrix A(2,4, CblasColMajor);
   Matrix B(3,4, CblasColMajor);
   //Matrix D(B,0,1,2,2);
   Matrix C(2,3, CblasRowMajor);
   //Matrix C(A,1,0,2,3);
   
   double a=0.0;
   double *data = A.data();
   
   for(size_t i=0;i<A.rows*A.cols;i++) {
      data[i] = a;
      a += 1.0;
   }

   a = 0.0;
   data = B.data();
   for(size_t i=0;i<B.rows*B.cols;i++) {
      data[i] = a;
      a += 1.0;
   }

   Matrix D = A*B.transpose();
   
   //A->set_zero();
   
   /*a = 0.0;
   for(size_t i=0;i<A->rows;i++) {
      for(size_t j=0;j<A->cols;j++) {
         A->set(i,j,a);
         a += 1.0;
      }
   }*/
   
   
   
   cout << A << "\n";
   cout << B << "\n";
   cout << D << "\n";
   
   Matrix::dgemm(1.0,A,B.transpose(),0.0,C);
   
   cout << C << "\n";
   
   //C = B + A;
   //B += A;
   //C.set(0,0,4.0);
   
   /*
   cout << B << "\n";
   //cout << C << "\n";
   //cout << C << "\n";
   
   
   Matrix D = C.transpose();
   
   cout << C << "\n";
   cout << D << "\n";
   
   Matrix E(D,1,0,2,2);
   
   cout << E << "\n";

   Matrix B = Matrix(A,0,1,3,2);
   
   cout << B << "\n";
   
   B.set(0,0,14);
   cout << B << "\n";*/
   
   Vector v1(3);
   v1.set_zero();

   a = 0.0;
   for(size_t i=0;i<v1.size;i++) {
      v1(i) = a;
      a += 1.0;
   }

   
   cout << "v1:\n" << v1 << "\n";

   Vector v2(v1,1,2);
   cout << "v2:\n" << v2 << "\n";
   
   cout << "v2*v2 = " << Vector::dot(v2,v2) << "\n";
   
   return 0;
}

