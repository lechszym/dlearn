include_directories(${dlearn_SOURCE_DIR}/include)
include_directories(${GSL_INCLUDE_DIR})
include_directories(${CMAKE_SOURCE_DIR}/dmatrix/src)

add_executable(dtrain fblock.cpp fLogsig.cpp Dataset.cpp test.cpp)

add_executable(test_mlff fblock.cpp fLogsig.cpp Dataset.cpp dIris.cpp test_mlff.cpp)

add_executable(test_dmatrix test.cpp)

#set_property(TARGET dtrain PROPERTY CXX_STANDARD 11)
add_library(dmatrix STATIC 
${CMAKE_SOURCE_DIR}/dmatrix/src/MatrixBase.cpp
${CMAKE_SOURCE_DIR}/dmatrix/src/Matrix.cpp
${CMAKE_SOURCE_DIR}/dmatrix/src/Vector.cpp)

target_link_libraries(dtrain -L${GSL_LIB} -lgsl dmatrix)
target_link_libraries(test_mlff -L${GSL_LIB} -lgsl dmatrix)
target_link_libraries(test_dmatrix -L${GSL_LIB} -lgsl dmatrix)

if(BLAS_FOUND)
        target_link_libraries(dtrain ${BLAS_LIBRARIES})
        target_link_libraries(test_mlff ${BLAS_LIBRARIES})
        target_link_libraries(test_dmatrix ${BLAS_LIBRARIES})
elseif(Openblas_FOUND)
        target_link_libraries(dtrain ${Openblas_LIBRARIES})
        target_link_libraries(test_mlff ${Openblas_LIBRARIES})
        target_link_libraries(test_dmatrix ${Openblas_LIBRARIES})
else(GSL_FOUND)
        target_link_libraries(dtrain -lgslcblas)
        target_link_libraries(test_mlff -lgslcblas)
        target_link_libraries(test_dmatrix -lgslcblas)
endif(BLAS_FOUND)



#if(GSL_FOUND)
#add_executable(test_speed_gslcblas MatrixBase.cpp Matrix.cpp Vector.cpp test_speed.cpp)
#target_link_libraries(test_speed_gslcblas -L${GSL_LIB} -lgslcblas)
#endif(GSL_FOUND) 

#if(Openblas_FOUND)
#add_executable(test_speed_openblas MatrixBase.cpp Matrix.cpp Vector.cpp test_speed.cpp)
#target_link_libraries(test_speed_openblas ${Openblas_LIBRARIES})
#endif(Openblas_FOUND) 

#if(BLAS_FOUND)
#add_executable(test_speed_blas MatrixBase.cpp Matrix.cpp Vector.cpp test_speed.cpp)
#target_link_libraries(test_speed_blas ${BLAS_LIBRARIES})
#endif(BLAS_FOUND) 

add_custom_command(
        TARGET dtrain POST_BUILD
        COMMAND ln -sf ${cppnet_SOURCE_DIR}/data/ ../data
)
