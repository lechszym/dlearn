/* 
 * File:   fNorm.h
 * Author: lechadmin
 *
 * Created on October 29, 2015, 9:51 AM
 */

#ifndef FNORM_H
#define	FNORM_H

#include "Block.h"

namespace dlearn {

class fNorm : public learner {
public:
   fNorm(double min, double max);
   fNorm(const fNorm& orig);
   virtual ~fNorm();
private:
   double _min;
   double _max;
   
};
}

#endif	/* FNORM_H */

