/* 
 * File:   fLin.h
 * Author: lechadmin
 *
 * Created on November 23, 2015, 3:22 PM
 */

#ifndef FLIN_H
#define	FLIN_H

#include "fblock.h"

namespace dlearn {

class fLin : public fblock {
public:
   
   fLin(size_t nOutputs) {
      _outputs = nOutputs;
   }

   size_t _nParams() const {
      return (_inputs+1)*_outputs;
   }

   void f(const dmatrix::Vector& w, const dmatrix::Matrix& X, dmatrix::Matrix &Y) {
      dmatrix::Matrix W = dmatrix::Matrix(w, _inputs, _outputs);
      dmatrix::Vector b = dmatrix::Vector(w,_inputs*_outputs,_outputs);

      for(size_t i=0;i<Y.cols;i++) {
         Y.column(i).copy(b);
      }

      dmatrix::Matrix::dgemm(1.0, W, X, 0.0, Y);
   }

   void dfdx(const dmatrix::Vector& w, const dmatrix::Matrix& X, const dmatrix::Matrix &Y, dmatrix::Matrix& dYdX) {

   }   

};
}

#endif	/* FLIN_H */

