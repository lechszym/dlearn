/* 
 * File:   Dataset.cpp
 * Author: lechszym
 * 
 * Created on 26 April 2015, 10:07 AM
 */

#include "Dataset.h"

using namespace dmatrix;

namespace dlearn {

Dataset::Dataset()  {
}


Dataset::Dataset(size_t xDim, size_t tDim, size_t N) : Dataset() {
   X = Matrix(xDim,N);
   T = Matrix(tDim,N);
}


size_t Dataset::nInputs() const {
   return 0;
}


Dataset::Dataset(const Dataset& orig) {
}

Dataset::~Dataset() {
}


Matrix Dataset::x() {
   return this->X;
}

Matrix Dataset::t() {
   return this->T;
}

}